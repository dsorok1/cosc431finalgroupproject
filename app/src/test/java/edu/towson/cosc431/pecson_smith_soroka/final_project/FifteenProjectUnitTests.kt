package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.content.Context
import android.os.AsyncTask
import android.provider.ContactsContract
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.*
import org.json.JSONObject
import org.junit.Test

import org.junit.Assert.*
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.coroutineContext

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class FifteenProjectUnitTests {


    val testPro1 = Profile(1,"foo",1,0)

    val testPro2 = Profile(1,"bar",0,0)

    val testPro3 = Profile(1,"baz",0,0)

    val pros = listOf(testPro1,testPro2,testPro3)

    val incorrectTriviaAnswer = TriviaAnswer("foo", false)

    val correctTriviaAnswer = TriviaAnswer("bar", true)

    val triviaCategory1 = TriviaCategory(0, "foo")

    val triviaCategory2 = TriviaCategory(1, "bar")

    val quizData1 = QuizData(0, "name")

    val quizData2 = QuizData(1, "name")

    val triviaQuestion1 =  TriviaQuestion(1,
                              "category",
                              "type",
                              "difficulty",
                              "question",
                              mutableListOf(correctTriviaAnswer, incorrectTriviaAnswer))

    val triviaQuestion2 =  TriviaQuestion(2,
        "category",
        "type",
        "difficulty",
        "question",
        mutableListOf(correctTriviaAnswer, incorrectTriviaAnswer))

    //1
    @Test
    fun test_empty_profile() {
        val Profile = Profile(0,"",0,0)
        assert(Profile.name.isBlank())
    }

    //2
    @Test
    fun test_filter_profile_by_name(){
        val theNameDesired = "baz"
        pros.filter { x -> x.name==theNameDesired}
    }

    //3
    @Test
    fun test_wins_losses() {
        val wins = 5
        val losses = 5
        assert(wins>=losses)
    }

    //4
    @Test
    fun test_notifyHighScore(){
        val wins = 0
        val checkHighScore = pros.filter{x->x.wins > wins}
        assert(checkHighScore.size==1)
    }

    //5
    @Test
    fun test_create_new_profile_requirement(){
        val invalidProfile = ""
        assert(invalidProfile.isBlank())
    }

    //6
    @Test
    fun test_get_clear_user_profile_db_wins_losses(){
        val pro = testPro1
        val clearedProfile = Profile(pro.id,pro.name,0,0)
        assert(clearedProfile.wins==0&&clearedProfile.losses==0)

    }

    //7
    @Test
    fun test_register_wins(){
        val tPro = testPro1
        val updatedPro = Profile(tPro.id,tPro.name,tPro.wins+1,tPro.losses)
        assert(tPro.id==updatedPro.id &&tPro.name==updatedPro.name&& tPro.wins+1 == updatedPro.wins && tPro.losses==updatedPro.losses)
    }

    //8
    @Test
    fun test_register_losses(){
        val tPro = testPro1
        val updatedPro = Profile(tPro.id,tPro.name,tPro.wins,tPro.losses+1)
        assert(tPro.id==updatedPro.id &&tPro.name==updatedPro.name&& tPro.wins == updatedPro.wins && tPro.losses+1==updatedPro.losses)
    }

    //9
    @Test
    fun test_incorrect_answer(){
        val tAns = incorrectTriviaAnswer
        assert(tAns.isCorrect == false)
    }

    //10
    @Test
    fun test_correct_answer(){
        val tAns = correctTriviaAnswer
        assert(tAns.isCorrect == true)
    }

    //11
    @Test
    fun test_unique_category_id(){
        val tCat1 = triviaCategory1
        val tCat2 = triviaCategory2
        assert(tCat1.id != tCat2.id)
    }

    //12
    @Test
    fun test_get_category_by_id(){
        val tCat1 = triviaCategory1
        val idToGet = 0
        assert(tCat1.id == idToGet)
    }

    //13
    @Test
    fun test_timeout(){
        var time = 10
        var incorrect = false
        while(time >= 0)
        {
            if(time.equals(0))
                incorrect = true
            time--
        }

        assert(incorrect)
    }

    //14
    @Test
    fun test_quiz_data_unique(){
        val tQuizData1 = quizData1
        val tQuizData2 = quizData2
        assert(tQuizData1.id != tQuizData2.id)
    }

    //15
    @Test
    fun test_question_unique(){
        val tQuest1 = triviaQuestion1
        val tQuest2 = triviaQuestion2
        assert(tQuest1 != tQuest2)
    }
}
