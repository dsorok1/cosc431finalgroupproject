package edu.towson.cosc431.pecson_smith_soroka.final_project.databases

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile

//Layout of the database
//TODO:  Add new columns to me, I am the profiles tables
object ProfileContract{
    object ProfileEntry : BaseColumns{
        const val TABLE_NAME = "profiles"
        const val COLUMN_NAME_NAME = "name"
        const val COLUMN_NAME_WINS = "wins"
        const val COLUMN_NAME_LOSSES = "losses"
        const val COLUMN_NAME_DELETED = "deleted"
    }
}

//SQL db table creation
private const val SQL_CREATE_ENTRIES =
    "CREATE TABLE ${ProfileContract.ProfileEntry.TABLE_NAME} (" +
            "${BaseColumns._ID} INTEGER," +
            "${ProfileContract.ProfileEntry.COLUMN_NAME_NAME} TEXT," +
            "${ProfileContract.ProfileEntry.COLUMN_NAME_WINS} INTEGER," +
            "${ProfileContract.ProfileEntry.COLUMN_NAME_LOSSES} INTEGER," +
            "${ProfileContract.ProfileEntry.COLUMN_NAME_DELETED} INTEGER DEFAULT 0)"

private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${ProfileContract.ProfileEntry.TABLE_NAME}"

class ProfileDatabase(ctx: Context) : IProfileDatabase {
    override fun deleteProfile(p: Profile) {
        /*val cvs = ContentValues()
        cvs.put(ProfileContract.ProfileEntry.COLUMN_NAME_DELETED, "1")
        db.update(
            ProfileContract.ProfileEntry.TABLE_NAME,
            cvs,
            "${BaseColumns._ID} = ?",   // where clause
            //TODO CHANGE ME
            arrayOf("${p.name}")// where args
        )*/


    }

    override fun updateProfile(p:Profile): Boolean{
        val cvs = toContentValues(p)
        db.update(ProfileContract.ProfileEntry.TABLE_NAME,cvs,"_id= ${p.id}",null)
        return true
    }
    override fun addProfile(p: Profile) {
        val values = toContentValues(p)
        db.insert(ProfileContract.ProfileEntry.TABLE_NAME, null, values)
    }

    override fun getProfiles(): List<Profile> {
        val projection = arrayOf(BaseColumns._ID, ProfileContract.ProfileEntry.COLUMN_NAME_NAME) //, ProfileContract.ProfileEntry.COLUMN_NAME_AGE  Is this complete?

        // Sort by ID
        val sortOrder = "${BaseColumns._ID} DESC"
        val cursor = db.rawQuery( "SELECT * FROM profiles",null)


        //Add the profile to a mutableList, result
        val result = mutableListOf<Profile>()
        with(cursor) {
            while(cursor.moveToNext()) {

                val id = getInt(getColumnIndex(BaseColumns._ID))
                val name = getString(getColumnIndex(ProfileContract.ProfileEntry.COLUMN_NAME_NAME))
                val wins = getInt(getColumnIndex(ProfileContract.ProfileEntry.COLUMN_NAME_WINS))
                val losses = getInt(getColumnIndex(ProfileContract.ProfileEntry.COLUMN_NAME_LOSSES))
                result.add(Profile(id, name, wins, losses))
            }

        }
        return result
    }


    private fun toContentValues(p: Profile): ContentValues {
        val cv = ContentValues()
        cv.put(BaseColumns._ID, p.id)
        cv.put(ProfileContract.ProfileEntry.COLUMN_NAME_NAME, p.name)
        cv.put(ProfileContract.ProfileEntry.COLUMN_NAME_WINS, p.wins)
        cv.put(ProfileContract.ProfileEntry.COLUMN_NAME_LOSSES, p.losses)
        return cv
    }

    //Private instance of the database
    private val db: SQLiteDatabase

    //Initialize as a writable database
    init {
        db = ProfileDbHelper(ctx).writableDatabase
    }


    class ProfileDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION)
    {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(SQL_CREATE_ENTRIES)

        }

        //TODO: ??

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            if(newVersion == 2) {
                db?.execSQL("ALTER TABLE ${ProfileContract.ProfileEntry.TABLE_NAME} " +
                        "ADD COLUMN ${ProfileContract.ProfileEntry.COLUMN_NAME_DELETED} " +
                        "DEFAULT 0")
            } else {
                db?.execSQL(SQL_DELETE_ENTRIES)
                onCreate(db)
            }
        }

        companion object {
            val DATABASE_NAME = "profile.db"
            val DATABASE_VERSION = 2
        }
    }
}

