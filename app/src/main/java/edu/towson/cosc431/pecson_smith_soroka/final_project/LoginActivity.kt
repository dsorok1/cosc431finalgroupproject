package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileCollection
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileController
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileCollection
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.profile_item.*
import kotlinx.android.synthetic.main.profile_login_fragment.*

class LoginActivity : AppCompatActivity(), IProfileController {


    //Instance of db to add profiles to
    private lateinit var db: ProfileDatabase

    //override val profileList: MutableList<Profile> = mutableListOf()
    lateinit var profileList: IProfileCollection


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        profileList = ProfileCollection()
        db = ProfileDatabase(this)
        profileList.addProfiles(db.getProfiles())
        CT_login_save.setOnClickListener{launchHomeScreen()}
        CT_login_back.setOnClickListener { launchMainScreen() }
    }

    private fun launchMainScreen() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    private fun launchHomeScreen() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }
    override fun getProfile(idx: Int): Profile {
        return profileList.getProfile(idx) //To change body of created functions use File | Settings | File Templates.
    }


    //Returns length of profileList
    override fun profileCount(): Int {
        return profileList.getCount()
    }

    //Retrieves list of profile (NOTE:  This method returns the list of all profiles for the recycler view
    override fun getProfiles(): List<Profile> {
        return profileList.getAll()
    }
}