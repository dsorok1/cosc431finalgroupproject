package edu.towson.cosc431.pecson_smith_soroka.final_project.adapters
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import edu.towson.cosc431.pecson_smith_soroka.final_project.R
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.view_holders.ProfileViewHolder

import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileController
import edu.towson.cosc431.pecson_smith_soroka.final_project.view_holders.AchievementsViewHolder
import kotlinx.android.synthetic.main.profile_item.view.*

//Achievements Adapter inflates the RecyclerView for Achievements
class AchievementsAdapter(private val controller: IProfileController) : RecyclerView.Adapter<AchievementsViewHolder>() {



    //Retrieve the amount of profiles
    override fun getItemCount(): Int {
        return controller.profileCount()
    }

    //Inflate the Achievements ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AchievementsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.profile_achievements_grid_items, parent, false)
        val holder = AchievementsViewHolder(view)
        return holder
    }

    //Bind the profile values displayed to cards in the recyclerview
    override fun onBindViewHolder(holder: AchievementsViewHolder, position: Int) {

        val profile = controller.getProfile(position)
        holder.bind(profile)
    }


}


