package edu.towson.cosc431.pecson_smith_soroka.final_project

/*
    TODO: add error handling for interrupted network calls,
    TODO: separate network call into own file,
    TODO: get stats on questions answered
            ex. time taken on each
    TODO: don't show default layout before done loading
    TODO: fix how it sends avg time
 */

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.PorterDuff
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.text.Html
import android.util.Log
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.TriviaAnswer
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.TriviaQuestion
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.activity_quiz_results.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

//used for answer choices
enum class Answer{A, B, C, D, TIMEOUT, ERROR, INCORRECT}

object IntentTags{
    val EXTRA_CORRECT = "EXTRA_CORRECT"
    val EXTRA_INCORRECT = "EXTRA_INCORRECT"
    val EXTRA_TIMES = "EXTRA_TIMES"
}

class QuizActivity : AppCompatActivity() {

    private val TAG_INFO = "TAG_INFO"

    //redefine max later
    private var maximumQuestions = -1
    var currentQuestionNum = 0

    private val waitAfterQuestion = 1000
    //TODO: implement timer
    private var timer: CountDownTimer? = null
    private var timerCountdown = (15L * 1000L)
    private val timerStep = 5L
    private var timeLeftSec = -1.0
    private val timeSecTakenOnEachQuestion = ArrayList<Double>()


    private val listQuestions = ArrayList<TriviaQuestion>()
    private var correctAnswerAlpha = Answer.ERROR


    var correctNum = 0
    var incorrectNum = 0

    //from api
    private val JSON_CONFIRMATION_KEY = "response_code"
    private val JSON_KEY = "results"

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        btnBack.setOnClickListener{backBtnClick()}
        btnSkip.setOnClickListener{answerClicked(Answer.INCORRECT)}

        btnA.setOnClickListener{answerClicked(Answer.A)}
        btnB.setOnClickListener{answerClicked(Answer.B)}
        btnC.setOnClickListener{answerClicked(Answer.C)}
        btnD.setOnClickListener{answerClicked(Answer.D)}

        disableAllButtons()

        val data = intent

        /* OLD METHOD OF GETTING MAX QUESTIONS!
        if(data.hasExtra(QuizOptionsActivity.TAG_NUM_QUESTIONS))
        {//sets the max questions from previous
            maximumQuestions = data.getIntExtra(QuizOptionsActivity.TAG_NUM_QUESTIONS, 10)
        }*/

        if(data.hasExtra("json"))
        {
            //used in old method
            //val url = data.getStringExtra(RestIntentService.URL)
            //Log.d("TRIVIA", url)
            val json = getIntent().getStringExtra("json")
            Log.d("TRIVIA", json)
            handleJson(json)

            //AsyncTaskHandleJson().execute(url)
        }
        //reloadScreen()

        //timer things
        timer = object: CountDownTimer(timerCountdown, timerStep){
            override fun onTick(millisUntilFinished: Long) {
                progressBar.progress = progressBar.max - ((100 * millisUntilFinished/timerCountdown).toInt())
                timeLeftSec = millisUntilFinished/(1000.0)
                textTime.text = String.format("%.2f", millisUntilFinished/(1000L).toDouble())
            }

            override fun onFinish() {
                textTime.text = String.format("%.2f", (0).toDouble())
                timeLeftSec = 0.0
                answerClicked(Answer.TIMEOUT)
            }
        }
        progressBar.max = 100

        reloadScreen()
    }

    //TODO: make new kotlin file to handle this stuff
    //this is copied and pasted from QuizOptionsActivity (there has to be a better way!)
    /*
    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg url: String?): String {
            var text: String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try{
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader -> reader.readText()}}
            }
            finally {
                connection.disconnect()
            }

            return text
        }

        @TargetApi(Build.VERSION_CODES.M)
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onPostExecute(result: String?)
        {
            super.onPostExecute(result)
            handleJson(result)

            reloadScreen()
        }
    }*/

    private fun handleJson(jsonString : String?)
    {
        //convert entire response to json object
        val jsonObj = JSONObject(jsonString)
        Log.d(TAG_INFO, "jsonObj = " + jsonObj.toString())

        //get the response code
        val jsonResponseCode = jsonObj.optInt(JSON_CONFIRMATION_KEY)
        Log.d(TAG_INFO, "jsonResponseCode = " + jsonResponseCode)


        val jsonQuestions = jsonObj.optJSONArray(JSON_KEY)
        Log.d(TAG_INFO, "jsonQuestions = " + jsonQuestions.toString())

        //val listQuestions = ArrayList<TriviaQuestion>()

        //sets max questions to be the length of the questions array, makes sense
        maximumQuestions = jsonQuestions.length()

        var itr = 0
        while (itr < jsonQuestions.length())
        {
            //jsonObject holds the entire json for a question
            val jsonObject = jsonQuestions.getJSONObject(itr)

            //adds info to make new questions int the list
            listQuestions.add(TriviaQuestion(
                itr,
                jsonObject.getString("category"),
                jsonObject.getString("type"),
                jsonObject.getString("difficulty"),
                Html.fromHtml(jsonObject.getString("question")).toString(),
                makePotentialAnswerList(
                    Html.fromHtml(jsonObject.getString("correct_answer")).toString(),
                    makeIncorrectList(jsonObject.optJSONArray("incorrect_answers")))
            ))

            itr++
        }

        Log.d(TAG_INFO, listQuestions.toString())
    }

    //takes json array of incorrect answers and turns it into a string list
    private fun makeIncorrectList(jsonIncorrectArray: JSONArray): List<String>
    {
        var itr = 0
        val incorrectList = ArrayList<String>()
        while(itr < jsonIncorrectArray.length())
        {
            incorrectList.add(
                (Html.fromHtml((jsonIncorrectArray[itr].toString()))).toString()
            )
            itr++
        }
        return incorrectList
    }

    //combines the correct answer and incorrect answers to make a list of all answer types
    private fun makePotentialAnswerList(correctAnswer: String, incorrectAnswers: List<String>): List<TriviaAnswer>
    {
        val TAlist = ArrayList<TriviaAnswer>()
        TAlist.add(TriviaAnswer(correctAnswer, true))

        var itr = 0
        while(itr < incorrectAnswers.size)
        {
            TAlist.add(TriviaAnswer(incorrectAnswers[itr], false))

            itr++
        }

        return TAlist
    }





    private fun answerIsCorrect(ans: Answer): Boolean
    {
        return ans == correctAnswerAlpha
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun answerClicked(ans : Answer){

        Log.d(TAG_INFO, "Question ${currentQuestionNum}, answer clicked: ${ans}, correct answer: ${correctAnswerAlpha}," +
                " time taken to answer = ${(timerCountdown/1000.0) - timeLeftSec}")

        timeSecTakenOnEachQuestion.add((timerCountdown/1000.0) - timeLeftSec)
        timeLeftSec = -1.0

        if(answerIsCorrect(ans))
        {//increment correct
            correctNum++
            timer?.cancel()
        }
        else
        {//increment incorrect number and show red on selected answer
            incorrectNum++
            when(ans)
            {
                Answer.A -> btnA.background.setColorFilter(getColor(R.color.incorrectAnswer), PorterDuff.Mode.MULTIPLY)
                Answer.B -> btnB.background.setColorFilter(getColor(R.color.incorrectAnswer), PorterDuff.Mode.MULTIPLY)
                Answer.C -> btnC.background.setColorFilter(getColor(R.color.incorrectAnswer), PorterDuff.Mode.MULTIPLY)
                Answer.D -> btnD.background.setColorFilter(getColor(R.color.incorrectAnswer), PorterDuff.Mode.MULTIPLY)
            }
        }
        // and show green on correct answer & wait a split second

        when(correctAnswerAlpha)
        {
            Answer.A -> btnA.background.setColorFilter(getColor(R.color.correctAnswer), PorterDuff.Mode.MULTIPLY)
            Answer.B -> btnB.background.setColorFilter(getColor(R.color.correctAnswer), PorterDuff.Mode.MULTIPLY)
            Answer.C -> btnC.background.setColorFilter(getColor(R.color.correctAnswer), PorterDuff.Mode.MULTIPLY)
            Answer.D -> btnD.background.setColorFilter(getColor(R.color.correctAnswer), PorterDuff.Mode.MULTIPLY)
        }

        //disable all buttons so you can't screw up the measurements
        disableAllButtons()
        timer?.cancel()

        //have to call a short delay
        //val task = BackgroundTimer(waitAfterQuestion)
        //task.execute()
        Handler().postDelayed({
            Log.d(TAG_INFO, "In postDelayed")
            currentQuestionNum++

            reloadScreen()
        }, waitAfterQuestion.toLong())

        //TODO: fix above code and stop timer here also

        //then increment # of questions answered
        //currentQuestionNum++

        //reloadScreen()
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun reloadScreen()
    {
        Log.d(TAG_INFO, "currentQuestionNum = ${currentQuestionNum}, maximumQuestions = ${maximumQuestions}")
        if(currentQuestionNum >= maximumQuestions)
        {//that means go onto the new screen
            launchQuizResultsScreen()
            Log.d(TAG_INFO, "currentQuestionNum >= maximumQuestions")
            //launchQuizResultsScreen()
        }
        else {
            //using the new info of the round, update what is displayed
            textQuestionNum.text = "Question #" + (currentQuestionNum + 1)

            textQuestion.setText(listQuestions[currentQuestionNum].question)

            reloadButtons()
        }

        //have to restart timer
        timer?.start()
    }

    private fun enableAllButtons()
    {
        btnA.isEnabled = true
        btnB.isEnabled = true
        btnC.isEnabled = true
        btnD.isEnabled = true
        btnSkip.isEnabled = true
    }

    private fun disableAllButtons()
    {
        btnA.isEnabled = false
        btnB.isEnabled = false
        btnC.isEnabled = false
        btnD.isEnabled = false
        btnSkip.isEnabled = false
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun reloadButtons()
    {//have to do differently if answers are multiple choice VS true false

        //also first re-enable all buttons since the delay disables them
        enableAllButtons()

        //but, first reset color
        btnA.background.clearColorFilter()
        btnB.background.clearColorFilter()
        btnC.background.clearColorFilter()
        btnD.background.clearColorFilter()

        /*
        btnA.setBackgroundResource(android.R.drawable.btn_default)
        btnB.setBackgroundResource(android.R.drawable.btn_default)
        btnC.setBackgroundResource(android.R.drawable.btn_default)
        btnD.setBackgroundResource(android.R.drawable.btn_default)
        */

        if((listQuestions[currentQuestionNum].type).equals("multiple")) {
            val shuffledAnswers = (listQuestions[currentQuestionNum].potential_answers).shuffled()

            //correctAnswerAlpha now has the right answer value of the correct answer
            //shuffles on reload
            var itr = 0
            while (itr < shuffledAnswers.size) {
                if (shuffledAnswers[itr].isCorrect) {
                    correctAnswerAlpha = when (itr) {
                        0 -> Answer.A
                        1 -> Answer.B
                        2 -> Answer.C
                        3 -> Answer.D
                        else -> Answer.ERROR
                    }
                }

                itr++
            }

            val ansSize = shuffledAnswers.size

            btnA.setText(shuffledAnswers[0].value)
            btnB.setText(shuffledAnswers[1].value)

            btnC.setText(shuffledAnswers[2].value)
            //btnC.isEnabled = true
            btnD.setText(shuffledAnswers[3].value)
            //btnD.isEnabled = true
        }
        else
        {//answers are true/false

            val orderedAnswers = (listQuestions[currentQuestionNum].potential_answers).sortedByDescending { it.value }
            //should be ordered in reverse alphabetical order by value (aka true, then false)

            if(orderedAnswers[0].isCorrect)
            {//true is correct
                correctAnswerAlpha = Answer.A
            }
            else
            {//false is correct
                correctAnswerAlpha = Answer.B
            }

            btnA.setText(orderedAnswers[0].value)
            btnB.setText(orderedAnswers[1].value)

            btnC.setText("    ")
            btnC.isEnabled = false
            btnD.setText("    ")
            btnD.isEnabled = false
        }
    }

    //when go button is clicked
    private fun launchQuizResultsScreen() {
        //disable extra answering of questions
        disableAllButtons()

        val intent = Intent(this, QuizResultsActivity::class.java)
        intent.putExtra(IntentTags.EXTRA_CORRECT, correctNum)
        intent.putExtra(IntentTags.EXTRA_INCORRECT, incorrectNum)

        //calculate avg time taken
        intent.putExtra(IntentTags.EXTRA_TIMES, calculateAvgTime())

        //kill timer
        timer?.cancel()
        timer = null

        startActivity(intent)
    }

    private fun calculateAvgTime():Double
    {
        var totTime = 0.0
        var itr = 0
        while (itr < timeSecTakenOnEachQuestion.size)
        {
            totTime += timeSecTakenOnEachQuestion[itr]
            itr++
        }
        Log.d(TAG_INFO, "total time = ${totTime}, avg time = ${totTime/ ((timeSecTakenOnEachQuestion.size).toDouble())}")
        return (totTime/ ((timeSecTakenOnEachQuestion.size).toDouble()))
    }

    //when back button is clicked
    @RequiresApi(Build.VERSION_CODES.M)
    private fun backBtnClick() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this)

        // Display a message on alert dialog
        builder.setMessage("Are you sure you want to quit?")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Yes") { dialog, which ->
            // Do something when user press the positive button
            //launchHomeScreen()
            answerClicked(Answer.INCORRECT)
            launchQuizResultsScreen()
        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("No") { dialog, which -> }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }

    private fun launchHomeScreen() {
        val intent = Intent(this, HomeActivity::class.java)
        timer?.cancel()
        timer = null
        startActivity(intent)
    }
}
