package edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces

interface ICategoryController {
    fun selectCategory(categoryIndex: Int)
}