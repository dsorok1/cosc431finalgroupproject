package edu.towson.cosc431.pecson_smith_soroka.final_project.models

data class QuizData(val id: Int, val name: String)