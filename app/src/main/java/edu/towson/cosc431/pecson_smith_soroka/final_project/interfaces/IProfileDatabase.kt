package edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces

import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile

interface  IProfileDatabase{
    fun addProfile(p: Profile)
    fun getProfiles(): List<Profile>
    fun deleteProfile(p: Profile)
    fun updateProfile(p:Profile):Boolean
}