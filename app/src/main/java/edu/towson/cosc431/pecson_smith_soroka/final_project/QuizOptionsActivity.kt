package edu.towson.cosc431.pecson_smith_soroka.final_project

/*
    TODO: add error handling for interrupted network calls,
 */

import `mipmap-anydpi-v26`.CategoriesAdapter
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import kotlinx.android.synthetic.main.activity_quiz_options.*
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.ICategoryController
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.TriviaCategory
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import android.support.v4.content.LocalBroadcastManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.os.ResultReceiver


class QuizOptionsActivity : AppCompatActivity(), ICategoryController {

    enum class Difficulty{ANY, EASY, MEDIUM, HARD}

    enum class Type{MULTIPLE, BOOLEAN, EITHER}

    //need to load all categories to display in recycler view as buttons

    private lateinit var categories: ArrayList<TriviaCategory>

    private var currentCategory: TriviaCategory? = null
    private var currentDifficulty = Difficulty.ANY
    private var currentType = Type.MULTIPLE
    private var currentNumQuestions = 10

    private val ID_ALL_CATEGORIES = -1
    private val ID_ANY_CATEGORY = -2


    private val JSON_KEY = "trivia_categories"
    private val urlCategoryLookup = "https://opentdb.com/api_category.php"

    private val urlStub = "https://opentdb.com/api.php?"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_options)

        //control buttons
        btnBack.setOnClickListener{launchHomeScreen()}
        btnGo.setOnClickListener{launchQuizScreen()}

        //checkbox for type
        cbIncludeTrueFalse.setOnClickListener{setCurrentType()}
        /*rbMultiple.setOnClickListener{setCurrentType(Type.MULTIPLE)}
        rbTrueFalse.setOnClickListener{setCurrentType(Type.BOOLEAN)}
        rbEither.setOnClickListener{setCurrentType(Type.EITHER)}*/

        //start disabled to ensure that user selects a category
        btnGo.isEnabled = false

        handleJson(getIntent().getStringExtra("json"))

        //AsyncTaskHandleJson().execute(urlCategoryLookup)

        //val serviceIntent = Intent(this, RestIntentService::class.java)
        //serviceIntent.putExtra("url", urlCategoryLookup)

        //startService(serviceIntent)

        Log.d("TRIVIA", "")
    }

    private fun setCurrentDifficulty(diff : Difficulty)
    {
        currentDifficulty = diff
    }

    private fun setCurrentType()
    {
        //no arguments, get from checkbox (check enables mult & tf, uncheck disables)
        if(cbIncludeTrueFalse.isChecked)
            setCurrentType(Type.EITHER)
        else
            setCurrentType(Type.MULTIPLE)
    }
    //mostly a holdover from when the user had more freedom
    private fun setCurrentType(ty: Type)
    {
        currentType = ty
    }
/*
    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg url: String?): String {
            var text: String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try{
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader -> reader.readText()}}
            }
            finally {
                connection.disconnect()
            }

            return text
        }

        override fun onPostExecute(result: String?)
        {
            super.onPostExecute(result)
            handleJson(result)
        }
    }*/

    private fun handleJson(jsonString: String?)
    {
        val jsonObj = JSONObject(jsonString)

        //trivia_categories
        val jsonArray = jsonObj.optJSONArray(JSON_KEY)

        categories = ArrayList<TriviaCategory>()
        categories.add(TriviaCategory(ID_ALL_CATEGORIES, "All Categories"))
        categories.add(TriviaCategory(ID_ANY_CATEGORY, "Any Category"))

        var itr = 0
        while (itr < jsonArray.length())
        {
            val jsonObject = jsonArray.getJSONObject(itr)

            categories.add(TriviaCategory(
                jsonObject.getInt("id"),
                jsonObject.getString("name")

            ))

            itr++
        }

        val adapter = CategoriesAdapter(categories, this)

        rvCategories.adapter = adapter

        rvCategories.layoutManager = LinearLayoutManager(this)

    }




    //when back button is clicked
    private fun launchHomeScreen() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    //when go button is clicked
    private fun launchQuizScreen() {
        val urlCategoryLookup = "https://opentdb.com/api_category.php"
        val serviceIntent = Intent(this, RestIntentService::class.java)
        serviceIntent.putExtra(RestIntentService.URL, urlToPass())
        serviceIntent.putExtra(RestIntentService.METHOD, RestIntentService.QUIZ)

        startService(serviceIntent)

        //val intent = Intent(this, QuizActivity::class.java)
        //intent.putExtra(TAG_URL_LOOKUP, urlToPass())
        //bugged questions, sometimes doesn't return enough
        //intent.putExtra(TAG_NUM_QUESTIONS, currentNumQuestions)
        //startActivity(intent)
    }

    //called from adapter when item tapped
    override fun selectCategory(categoryIndex: Int) {
        currentCategory = categories[categoryIndex]
        //can !! because currentCategory was DEFINITELY made not null by this previous step
        SelectedCategoryText.text = currentCategory!!.name

        //also enable the go button
        btnGo.isEnabled = true
    }



    private fun urlToPass():String
    {
        //get info from currently selected items
        var url = urlStub

        url += ("amount=" + currentNumQuestions)

        //if the current category is not -1 or -2 (all categories or any category), then use that in the url
        if(currentCategory!!.id != ID_ALL_CATEGORIES)
        {//does NOT equal -1, could still equal -2

            url += "&category="

            if(currentCategory!!.id == ID_ANY_CATEGORY)
            {//have to get random non-negative id
                url += (getRandomCategory())
            }
            else
            {//currentCategory is just a normal category
                url += (currentCategory!!.id)
            }
        }

        when(currentDifficulty)
        {
            Difficulty.EASY -> url += ("&difficulty=easy")
            Difficulty.MEDIUM -> url += ("&difficulty=medium")
            Difficulty.HARD -> url += ("&difficulty=hard")
        }

        //use if user can select true/false vs multiple choice

        when(currentType)
        {
            Type.MULTIPLE -> url += ("&type=multiple")
            Type.BOOLEAN -> url += ("&type=boolean")
            //Type.EITHER -> //do nothing
        }


        return url
    }

    private fun getRandomCategory(): Int
    {//know that first 2 entries are all categories and any, so the rest are valid
        //assume there are at least 3 categories
        val subList = ArrayList<TriviaCategory>(categories.subList(2, categories.size))
        //this MAY!!!!! (may) be destructive to original list but i think it is not
        return (subList.shuffled())[0].id
    }

    companion object
    {
        val TAG_URL_LOOKUP = "url_lookup"
        val TAG_NUM_QUESTIONS = "num_questions"
    }
}