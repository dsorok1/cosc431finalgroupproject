package edu.towson.cosc431.pecson_smith_soroka.final_project.models


import android.content.Context
import android.content.SharedPreferences
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileSelectionPreferences


//Source: http://www.kotlincodes.com/kotlin/shared-preferences-with-kotlin/
class ProfileSelectionPreferences(val context: Context): IProfileSelectionPreferences{

    private val PROFILE_NAME = "profileSelection"
    val sharedPref: SharedPreferences = context.getSharedPreferences(PROFILE_NAME, Context.MODE_PRIVATE)


    override fun profileSelection(KEY_NAME: String, profile: String) {
        clearSharedPreference()
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, profile!!)

        editor.commit()

    }

    override fun getProfileSerial(KEY_NAME: String): String? {

        return sharedPref.getString(KEY_NAME, null)
    }

    override fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.commit()
    }

    override fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.remove(KEY_NAME)
        editor.commit()
    }
}
