package edu.towson.cosc431.pecson_smith_soroka.final_project.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import edu.towson.cosc431.pecson_smith_soroka.final_project.R
import java.lang.IllegalStateException

class NoProfileDialogFragment : DialogFragment(){
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.Create_Profile_Title)
                .setNeutralButton(R.string.Please_Select_A_Profile,
                    {dialog, id ->
                    })
            builder.create()
        }?: throw IllegalStateException("Null Activity")
    }

}