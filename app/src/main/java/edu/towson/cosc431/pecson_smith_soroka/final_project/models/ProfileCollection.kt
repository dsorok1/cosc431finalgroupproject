package edu.towson.cosc431.pecson_smith_soroka.final_project.models

import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileCollection
import java.lang.Exception

class ProfileCollection(): IProfileCollection {

    private var profileList: MutableList<Profile> = mutableListOf()

    init{
        seed()
    }

    //Demonstration/Testing purposes:  Put Test Profiles in me
    private fun seed() {
    }

    //IF YOU DO NOT HAVE THE FOLLOWING METHODS YOUR POINTERS TO LIST OBJECTS WILL FAIL
    override fun getCount(): Int {
        return profileList.size
    }

    override fun getProfile(idx: Int): Profile {
        return profileList[idx]
    }

    override fun getAll(): List<Profile> {
        return profileList
    }
    override fun deleteProfile(idx:Int) {
       profileList.removeAt(idx)
    }

    override fun replace(idx: Int, profile: Profile) {
        if(idx >= profileList.size) throw Exception("There is no profile at this index")
        profileList[idx] = profile
    }

    override fun addProfile(profile: Profile) {
        profileList.add(profile)
    }

    override fun addProfiles(profiles: List<Profile>) {
        profileList.addAll(profiles)
    }

}