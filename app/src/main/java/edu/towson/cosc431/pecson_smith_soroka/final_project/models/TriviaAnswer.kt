package edu.towson.cosc431.pecson_smith_soroka.final_project.models

data class TriviaAnswer(val value: String,
                        val isCorrect: Boolean)