package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.fragments.NoProfileDialogFragment
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileCollection
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileController
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import kotlinx.android.synthetic.main.activity_createprofile.*

//Working to have this send the intent back to LoginActivity.kt
//TODO:  Make LoginActivity.kt receive person to add to profile list

class NewProfileActivity:AppCompatActivity() {

    private lateinit var db: ProfileDatabase


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_createprofile)


        CT_New_Profile_Back.setOnClickListener{launchMainScreen()}

        CT_New_Profile_Save.setOnClickListener{
            //If there is no input, pop alert.
            if(CT_Profile_Name_Entry.editableText.toString().isBlank()){
                alertD()
            }
            // Else add profile to db and launch main screen.
            else {
                getProfile()
                launchMainScreen()
            }
        }

    }

    private fun launchMainScreen() {
        val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
    }

    //Create profile
    private fun getProfile(): Profile {
        db = ProfileDatabase(this)
        //New profile created with the user input
        val newProfile = Profile(db.getProfiles().size,CT_Profile_Name_Entry.editableText.toString(),0,0)
        //Add profile to db
        db.addProfile(newProfile)
        return newProfile
    }

    //Alert if no user name is entered
    private fun alertD(){
        val frag = NoProfileDialogFragment()
        frag.show(supportFragmentManager,"alert")}
}