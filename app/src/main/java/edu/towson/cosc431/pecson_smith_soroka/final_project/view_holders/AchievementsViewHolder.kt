package edu.towson.cosc431.pecson_smith_soroka.final_project.view_holders

import android.support.v7.widget.RecyclerView
import android.view.View
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import kotlinx.android.synthetic.main.profile_achievements_grid_items.view.*

class AchievementsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    //Binds the profile name to the item view
    //Bind more values to me so that I can display more attributes about a given profile in a particular view
    fun bind(profile: Profile){
        itemView.profile_item_achievement_player_name.text = profile.name
        itemView.profile_item_achievement_record_wins.text = profile.wins.toString()
        itemView.profile_item_achievement_record_losses.text = profile.losses.toString()
    }

}