package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import kotlinx.android.synthetic.main.activity_quiz_results.*
import java.util.ArrayList

class QuizResultsActivity : AppCompatActivity() {

    //-1 used as a placeholder since lateinit is not allowed on primitive data types
    private var correctNum = -1
    private var incorrectNum = -1
    private var avgTime = -1.0

    private lateinit var db: ProfileDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_results)

        debug()
        getVariablesFromIntent()
        updateScreen()

        btnOk.setOnClickListener {
            //Data is only saved if the user exits to the home screen
            updateDB()//Update DB, also check for high score within
            launchHomeScreen() }
    }

    private fun launchHomeScreen() {
        val intent = Intent(this, HomeActivity::class.java)
        //Load homescreen
        startActivity(intent)
    }

    private fun debug() {
        Log.d("TRIVIA", "Correct = " + intent.getIntExtra("EXTRA_CORRECT", -1))
        Log.d("TRIVIA", "Inorrect = " + intent.getIntExtra("EXTRA_INCORRECT", -1))
    }


    //gets variables from intent
    private fun getVariablesFromIntent() {
        correctNum = intent.getIntExtra(IntentTags.EXTRA_CORRECT, -1)
        incorrectNum = intent.getIntExtra(IntentTags.EXTRA_INCORRECT, -1)
        avgTime = intent.getDoubleExtra(IntentTags.EXTRA_TIMES, -1.0)
    }

    //updates the screen using variables from the intent
    private fun updateScreen() {
        textCorrect.text = ("Correct = " + correctNum)
        textIncorrect.text = ("Incorrect = " + incorrectNum)
        textAvgTimeValue.text = (String.format("%.2f", avgTime.toDouble()) + " seconds")
        //savetoDB()
        //find percent correct
        val decimalCorrect = (correctNum.toDouble()) / ((correctNum + incorrectNum).toDouble())

        textPercent.text = ("% Correct = " + (decimalCorrect * 100.0).toString() + "%")
    }

    //Function to update DB
    private fun updateDB() {
        db = ProfileDatabase(this)
        //Get current user info
        val userProfile = getUserInfo()
        //Use that information (current user info) to find the corresponding profile in profiles.db
        val profile = getSpecificDBProfile(userProfile)
        //Did the user win or lose?
        if (registerWin(
                intent.getIntExtra(IntentTags.EXTRA_CORRECT, -1),
                intent.getIntExtra(IntentTags.EXTRA_INCORRECT, -1)))
        {
            //In case of winning
            //Increase the amount of wins by 1
            val updated = Profile(profile.id, profile.name, profile.wins + 1, profile.losses)
            //Account for High Score
            checkNewHighScore(updated.name, updated.wins)
            //Update Db with win
            db.updateProfile(updated)

        }
        //In case of losing
        else {
            //Increase amount of losses by 1
            val updated = Profile(profile.id, profile.name, profile.wins, profile.losses + 1)
            //Update Db with loss
            db.updateProfile(updated)

        }
    }

    //Return a specific profile from the db
    private fun getSpecificDBProfile(userProfile:String):Profile{
        return db.getProfiles().filter { x -> x.name == userProfile }.get(0)
    }

    //Return current user information
    private fun getUserInfo():String{
        val profileSelection: edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences =
            edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences(this)
        //Set Greeting to reflect user choice
        val userProfile = profileSelection.getProfileSerial("profile")
        return userProfile.toString()
    }

    //If the user answers five or more right, they win
    private fun registerWin(wins: Int, losses: Int): Boolean {
        return (wins >= losses)
    }

    //Take the profile name and their wins to check if there is a new high score champion
    private fun checkNewHighScore(name: String, wins:Int){
        //Grab all profiles to be viewed
        val profiles = db.getProfiles()
        //Filter profiles based on the current users wins
        val checkHighScore = profiles.filter { x -> x.wins > wins }
        //if there is nothing in that list of profiles you are the new high score champion
        if(checkHighScore.isEmpty())
            notifyHighScore(name)
    }

    //If a high score has been achieved, celebration is in order:  Notify User
    private fun notifyHighScore(newWinner:String) {
        //Open notification channel
        createNotificationChannel(this)

        //The builder uses channel "0", and data regarding the high score champion is set
        var builder = NotificationCompat.Builder(this, "0")
            .setSmallIcon(R.drawable.tooltip_frame_dark)
            .setContentTitle("High Score")
            .setContentText("The Crivia Track Champion is now: $newWinner")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        //The builder constructs the notification
        with(NotificationManagerCompat.from(this)) {
            notify(0, builder.build())
        }
    }

    //Create the notification channel
    private fun createNotificationChannel(context: Context) {
        //Check the android version and create the channel if necessary
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Set the channel name
            val name = context.getString(R.string.channel_name)
            //Set the description
            val descriptionText = context.getString(R.string.channel_description)
            //Set importance
            val importance = NotificationManager.IMPORTANCE_HIGH
           //Set channel id "0" with the above description, name and importance
            val channel = NotificationChannel("0", name, importance).apply {
                description = descriptionText
            }
            //Instantiate system service as notification manager
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            //Create the channel
            notificationManager.createNotificationChannel(channel)
        }
    }
}


