package edu.towson.cosc431.pecson_smith_soroka.final_project.models
/*
how the json appeat from API


{"category":"Entertainment: Comics",
"type":"multiple",
"difficulty":"easy",
"question":"Who is Batman?",
"correct_answer":"Bruce Wayne",
"incorrect_answers":["Clark Kent","Barry Allen","Tony Stark"]}
*/

data class TriviaQuestion(val num: Int,
                        val category: String,
                        val type: String,
                        val difficulty: String,
                        val question: String,
                        val potential_answers: List<TriviaAnswer>)
