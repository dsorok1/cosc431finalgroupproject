package edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces

import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile

interface IProfileController {
    //val profileList: MutableList<Profile>
    fun profileCount(): Int
    fun getProfile(idx: Int): Profile
    fun getProfiles(): List<Profile>
}