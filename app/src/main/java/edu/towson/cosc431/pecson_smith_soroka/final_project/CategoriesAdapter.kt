package `mipmap-anydpi-v26`

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.towson.cosc431.pecson_smith_soroka.final_project.R
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.ICategoryController
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.TriviaCategory
import kotlinx.android.synthetic.main.categories_layout.view.*

class CategoriesAdapter (val categories: List<TriviaCategory>, val controller: ICategoryController): RecyclerView.Adapter<CategoryViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        /*
        1. inflate view
        2. create viewholder
        3. *setup*
        4. return viewholder
        */
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.categories_layout, parent, false)
        val holder = CategoryViewHolder(view)

        //select category
        holder.itemView.setOnClickListener{
            //remember and display text

            val position = holder.adapterPosition
            controller.selectCategory(position)
        }

        return holder
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        //2 steps
        //1. get category out of list
        //2. set properties on viewholder
        val cat = categories.get(position)
        holder.itemView.txtSelectedCategory.text = cat.name//grabbing view that we passed it, set properties on view
    }

    override fun getItemCount(): Int {
        return categories.size
    }
}

class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view)