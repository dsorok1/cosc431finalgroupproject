package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings.Global.getString
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat.getSystemService
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //Access profile selection
        var profileSelection: edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences =
            edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences(this)
        //Set Greeting to reflect user choice
        CT_Home_Profile_Name.text = profileSelection.getProfileSerial("profile")


        CT_Home_Settings.setOnClickListener{launchSettingsScreen()}
        CT_Home_Achievements.setOnClickListener{loadAchievements()}
        CT_Home_Settings.setOnClickListener{loadSettings()}
        CT_Home_Back.setOnClickListener { loadMainActivity() }
        CT_Home_Start.setOnClickListener{loadQuizOptions()}

    }

    private fun loadMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    private fun loadAchievements(){
        val intent = Intent(this, AchievementsActivity::class.java)
        startActivity(intent)
    }
    //load quiz functionality
    private fun loadSettings(){
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    //load quiz functionality
    private fun loadQuizOptions(){
        val urlCategoryLookup = "https://opentdb.com/api_category.php"
        val serviceIntent = Intent(this, RestIntentService::class.java)
        serviceIntent.putExtra("url", urlCategoryLookup)
        serviceIntent.putExtra(RestIntentService.METHOD, RestIntentService.CATEGORIES)

        startService(serviceIntent)
        //val intent = Intent(this, QuizOptionsActivity::class.java)
        //startActivity(intent)
    }

    private fun launchSettingsScreen() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }


}