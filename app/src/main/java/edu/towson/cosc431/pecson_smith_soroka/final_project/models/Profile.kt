package edu.towson.cosc431.pecson_smith_soroka.final_project.models

data class Profile(val id: Int,
              val name: String,
              val wins: Int,
              val losses: Int) {
    fun name(): String{
        return name
    }

}
