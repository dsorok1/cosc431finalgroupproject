package edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces

//Interface for the
interface IProfileSelectionPreferences {
    fun profileSelection(KEY_NAME: String, profile: String)
    fun getProfileSerial(KEY_NAME: String):String?
    fun clearSharedPreference()
    fun removeValue(KEY_NAME: String)
}