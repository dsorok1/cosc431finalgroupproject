package edu.towson.cosc431.pecson_smith_soroka.final_project.view_holders


import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import kotlinx.android.synthetic.main.profile_item.view.*

class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    //Binds the profile name to the item view
    //Bind more values to me so that I can display more attributes about a given profile in a particular view
    fun bind(profile: Profile) {
        itemView.profile_item_name.text = profile.name

        //STORE USER CHOICE - When a given item view is clicked launch the profileSelection method
        itemView.setOnClickListener {
            profileSelection(itemView, profile)
        }
    }

    //Function for storing the user's profile choice
    fun profileSelection(view: View, profile: Profile) {

        //Variable accessing shared preferences
        var profileSelection: edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences =
            edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences(view.context)

        //Stored the profile name as the KEY_NAME "profile"
        profileSelection.profileSelection("profile", profile.name)

        //Demonstrate through the log that the profile choice is being stored in preferences
        Log.d(
            "Success" + ("profile"),
            profileSelection.getProfileSerial("profile")
        )
    }

}