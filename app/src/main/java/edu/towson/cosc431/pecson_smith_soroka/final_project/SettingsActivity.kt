package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import edu.towson.cosc431.pecson_smith_soroka.final_project.databases.ProfileDatabase
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity: AppCompatActivity(){
    private lateinit var db: ProfileDatabase
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        //Back button functionality
        CT_Settings_Back.setOnClickListener{
            launchHomeScreen()}
        CT_Settings_Clear.setOnClickListener {
            deleteBtnClick()}
    }
    private fun getSpecificDBProfile(userProfile:String):Profile{
        return db.getProfiles().filter { x -> x.name == userProfile }.get(0)
    }

    //Return current user information
    private fun getUserInfo():String{
        val profileSelection: edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences =
            edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileSelectionPreferences(this)
        //Set Greeting to reflect user choice
        val userProfile = profileSelection.getProfileSerial("profile")
        return userProfile.toString()
    }
    private fun updateDB() {
        db = ProfileDatabase(this)
        //Get current user info
        val userProfile = getUserInfo()
        //Use that information (current user info) to find the corresponding profile in profiles.db
        if(userProfile.isNullOrBlank()){

        }
        else {
            val profile = getSpecificDBProfile(userProfile)
            //Did the user win or lose?
            val updated = Profile(profile.id, profile.name, 0, 0)

            db.updateProfile(updated)

        }
    }

    //when delete button is clicked
    private fun deleteBtnClick()
    {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this)

        // Display a message on alert dialog
        builder.setMessage("Are you sure you want to clear data?")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Yes"){dialog, which ->
            // Do something when user press the positive button
            updateDB()
            launchHomeScreen()
        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("No"){dialog,which ->}

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }

    //Back button functionality
    private fun launchHomeScreen(){
        val intent = Intent(this,HomeActivity::class.java)
        startActivity(intent)
    }

    //TODO: Add Clear Data Functionality
}