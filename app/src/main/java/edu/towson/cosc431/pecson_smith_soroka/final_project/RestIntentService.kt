package edu.towson.cosc431.pecson_smith_soroka.final_project

import `mipmap-anydpi-v26`.CategoriesAdapter
import android.app.IntentService
import android.content.BroadcastReceiver
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.TriviaCategory
import kotlinx.android.synthetic.main.activity_quiz_options.*
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

@RequiresApi(Build.VERSION_CODES.CUPCAKE)
class RestIntentService : IntentService("RestIntentService")
{

    private val JSON_KEY_CATEGORY = "trivia_categories"

    override fun onHandleIntent(intent: Intent) {
        Log.d("IntentService", "In onHandleIntent")

        val method = intent.getStringExtra(METHOD)
        val url = intent.getStringExtra(URL)

        var text: String
        val connection = URL(url).openConnection() as HttpURLConnection
        try{
            connection.connect()
            text = connection.inputStream.use{it.reader().use{ reader -> reader.readText()}}
        }
        finally {
            connection.disconnect()
        }

        if(method.equals(CATEGORIES))
            handleJsonCategories(intent, text)
        else if(method.equals(QUIZ))
            handleJsonQuiz(intent, text)
    }

    private fun handleJsonCategories(intent: Intent, text: String)
    {
        //launch new activity to handle the json
        Log.d("IntentService", intent.toString())
        Log.d("IntentService", text)

        val launchIntent = Intent(this, QuizOptionsActivity::class.java)
        launchIntent.putExtra("json", text)
        startActivity(launchIntent)
    }

    private fun handleJsonQuiz(intent: Intent, text: String)
    {
        //launch new activity to handle json
        //relaunch old activity to handle the categories
        Log.d("IntentService", intent.toString())
        Log.d("IntentService", text)

        val launchIntent = Intent(this, QuizActivity::class.java)
        launchIntent.putExtra("json", text)
        startActivity(launchIntent)
    }

    companion object consts
    {
        val URL = "url"
        val METHOD = "method"
        val CATEGORIES = "categories"
        val QUIZ = "quiz"
    }

}