package edu.towson.cosc431.pecson_smith_soroka.final_project.adapters


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import edu.towson.cosc431.pecson_smith_soroka.final_project.R
import edu.towson.cosc431.pecson_smith_soroka.final_project.view_holders.ProfileViewHolder

import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileController
import kotlinx.android.synthetic.main.profile_item.view.*

//Profile Adapter inflates the RecyclerView for Profile Selection
class ProfileAdapter(private val controller: IProfileController) : RecyclerView.Adapter<ProfileViewHolder>() {

    //Retrieve amount of profiles
    override fun getItemCount(): Int {
        return controller.profileCount()
    }

    //Inflate the ProfileViewHolder with profile items
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.profile_item, parent, false)
        val holder = ProfileViewHolder(view)
        return holder
    }

    //Bind the profile values displayed to cards in the recyclerview
    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        val profile = controller.getProfile(position)
        holder.bind(profile)
    }


}



