package edu.towson.cosc431.pecson_smith_soroka.final_project.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.towson.cosc431.pecson_smith_soroka.final_project.R
import edu.towson.cosc431.pecson_smith_soroka.final_project.adapters.ProfileAdapter
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileController
import kotlinx.android.synthetic.main.profile_item_list.*
import kotlinx.android.synthetic.main.profile_login_fragment.*

//This is the parent fragment to ProfileDetailFragment and ProfileRepositoryFragment

class LoginFragment: Fragment() {
    private lateinit var controller: IProfileController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.profile_login_fragment, container, false)

        return view
    }

    override fun onAttach(context: Context?){

        super.onAttach(context)
        if(context is IProfileController){
            controller = context
        }
        else{
            Log.d("error","error")
        }
    }

    //Recycler View for the Fragment
    override fun onActivityCreated(savedInstanceState: Bundle?){
        super.onActivityCreated(savedInstanceState)
        val adapter = ProfileAdapter(controller)

        profileListView.adapter = adapter
        profileListView.layoutManager = LinearLayoutManager(activity)
    }

}