package edu.towson.cosc431.pecson_smith_soroka.final_project

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IMainController
import edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces.IProfileCollection
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile
import edu.towson.cosc431.pecson_smith_soroka.final_project.models.ProfileCollection
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IMainController {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //OnClickListeners for Main Activity Buttons
        CT_Main_Create_Profile.setOnClickListener { launchProfileWizard() }

        CT_Main_Login.setOnClickListener { launchLoginScreen() }
    }

    override fun launchHomeScreen() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    //Launch Create Profile Screen upon button press
    private fun launchProfileWizard() {
        val intent = Intent(this, NewProfileActivity::class.java)
        startActivity(intent)
    }

    //Launch Login screen upon button press
    private fun launchLoginScreen() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }


}

