package edu.towson.cosc431.pecson_smith_soroka.final_project.interfaces

import edu.towson.cosc431.pecson_smith_soroka.final_project.models.Profile

interface IProfileCollection {
    //List stored in ProfileCollection.kt
    fun getCount(): Int
    fun getProfile(idx:Int):Profile
    fun getAll(): List<Profile>
    fun deleteProfile(idx:Int)
    fun replace(idx: Int, profile: Profile)
    fun addProfile(profile: Profile)
    fun addProfiles(profiles: List<Profile>)}